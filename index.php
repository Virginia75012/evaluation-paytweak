<?php require_once 'Data/rsql.php';
require_once 'Data/info.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Paytweak</title>
</head>

<body>
    <header class="mt-3">
        <h1 class="text-center">Paytweak</h1>
        <hr/>
    </header>

<main class="container mt-5">
    <h3 class="text-center">Formulaire à renseigner</h3>

    <div class="col-offset-1">
        <form class="mt-5" id="form" method="post" action="">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="lastname">Nom<span class="blue"> *</span></label>
                    <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Votre Nom">
                    <p class="comments"></p>
                </div>

                <div class="form-group col-md-6">
                    <label for="firstname">Prénom<span class="blue"> *</span></label>
                    <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Votre Prénom">
                    <p class="comments"></p>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="adress">Adresse<span class="blue"> *</span></label>
                    <input type="text" name="adress" class="form-control" id="adress" placeholder="Votre adresse">
                    <p class="comments"></p>
                </div>

                <div class="form-group col-md-2">
                    <label for="postalcode">Code Postal<span class="blue"> *</span></label>
                    <input type="text" name="postalcode" class="form-control" id="postalcode" placeholder="Code postal">
                    <p class="comments"></p>
                </div>

                <div class="form-group col-md-4">
                    <label for="city">Ville<span class="blue"> *</span></label>
                    <input type="text" name="city" class="form-control" id="city" placeholder="Ville">
                    <p class="comments"></p>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="phone">Téléphone</label>
                    <input type="tel" name="phone" class="form-control" id="phone" placeholder="par ex&nbsp;: +33102030405">
                    <p class="comments"></p>
                </div>

                <div class="form-group col-md-6">
                    <label for="mobile">Mobile</label>
                    <input type="tel" name="mobile" class="form-control" id="mobile" placeholder="par ex&nbsp;: +33602030405">
                    <p class="comments"></p>
                </div>
            </div>

            <div class="message" col-md-12>
                <p class="blue"><em>* Ces informations sont requises</em></p>
            </div>

                <div class="container mt-5">
                    <button type="submit" class="btn btn-primary btn-block button1" value="Envoyer">Envoyer</button>
                </div>
        </form>
    </div>
</main>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>

