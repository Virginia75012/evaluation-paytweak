$(function(){
    console.log('init');

    $('#form').submit(function(evt){

        evt.preventDefault();
        $('.comments').empty();
        var postdata = $('#form').serialize();

        $.ajax({
            type: 'POST',
            url: 'Data/info.php',
            data: postdata,
            dataType: 'json',
            success: function(result){

                if(result.isSuccess){
                    $('#form').append('<p class="ok">Votre formulaire a bien été envoyé !</p>');
                    $('#form')[0].reset();
                }
                else{
                    $('#lastname + .comments').html(result.lastnameError);
                    $('#firstname + .comments').html(result.firstnameError);
                    $('#adress + .comments').html(result.adressError);
                    $('#postalcode + .comments').html(result.postalcodeError);
                    $('#city + .comments').html(result.cityError);
                    $('#phone + .comments').html(result.phoneError);
                    $('#mobile + .comments').html(result.mobileError);
                }
            }

        });
    });

})