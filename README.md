# Paytweak
## Procédure Evaluation Candidats


### Travail demandé

Créer une page web simple (index.php) comportant un formulaire avec les champs suivants :

| Nom       | Prénom     |              |
| --------- | -----------| ------------ |
| Adresse Complète | Code Postal | Ville |
| Téléphone | Mobile |

Les informations de ce formulaire doivent être envoyées en Ajax sur une seule ligne séparée par des pipes (« | ») dans un fichier texte « data.txt » dans un répertoire « données ».
Insérer également les données dans la base de donnée nommée « utilisateur » en créant la table « data » directement depuis l’ajax et non depuis la console MySQL Phpmyadmin, en vérifiant qu’elle n’existe pas avant.

__Toutes les fonctions qui concernent les requêtes MySQL doivent être dans un fichier séparé.__

A la fin de la requête Ajax les données doivent être automatiquement ajoutées à la volée en bas de la page Index à un tableau html basique sur une ligne.
Créer le tableau en 2 exemplaires : 1 qui récupère et modifie les données depuis le fichier data.txt, et 1 qui récupère et modifie les données depuis la base de donnée.

| Nom       | Prénom     | Adresse | Cp | Ville | Téléphone | Mobile | Boutons |
| ------- | ------- | ------ | ------- | ------ | ------ | ---- | ----|
|AAAAA|BBBB|CCCCC|12345|DDDDD|0102030405|0602030405| x x |

A partir de là :
- on doit pouvoir supprimer la ligne tout simplement du fichier et de la base
- on doit pouvoir éditer les informations  en cliquant sur le bouton de la ligne.
          (Retour dans le formulaire des données et réenregistrement par-dessus la ligne)