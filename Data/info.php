<?php
require_once 'rsql.php';

    $array = array(
        'lastname'=>"",
        'firstname'=>"",
        'adress'=>"",
        'postalcode'=>"",
        'city'=>"",
        'phone'=>"",
        'mobile'=>"",
        'lastnameError'=>"",
        'firstnameError'=>"",
        'adressError'=>"",
        'postalcodeError'=>"",
        'cityError'=>"",
        'phoneError'=>"",
        'mobileError'=>"",
        'isSuccess' => false
    );


    if($_SERVER['REQUEST_METHOD'] == 'POST'){

        $array['lastname'] = verifyInfo($_POST['lastname']);
        $array['firstname'] = verifyInfo($_POST['firstname']);
        $array['adress'] = verifyInfo($_POST['adress']);
        $array['postalcode']  = verifyInfo($_POST['postalcode']);
        $array['city'] = verifyInfo($_POST['city']);
        $array['phone'] = verifyInfo($_POST['phone']);
        $array['mobile'] = verifyInfo($_POST['mobile']);
        $array['isSuccess'] = true;

        if(empty($array['lastname'])){
            $array['lastnameError'] = "Veuillez renseigner votre nom";
            $array['isSuccess'] = false;
        }

        if(empty($array['firstname'])){
            $array['firstnameError'] = "Veuille renseigner votre prénom";
            $array['isSuccess'] = false;
        }

        if(empty($array['adress'])){
            $array['adressError'] = "Quel est votre adresse exacte ?";
            $array['isSuccess'] = false;
        }

        if(empty($array['postalcode'])){
            $array['postalcodeError'] = "Et le code postal ?";
            $array['isSuccess'] = false;
        }

        if(empty($array['city'])){
            $array['cityError'] = "Veuillez renseigner la ville";
            $array['isSuccess'] = false;
        }

        if(!isPhone($array['phone'])){
            $array['phoneError'] = "Renseignez que des chiffres ou des espaces";
            $array['isSuccess'] = false;
        }

        if(!isMobile($array['mobile'])){
            $array['mobileError'] = "Renseignez que des chiffres ou des espaces";
            $array['isSuccess'] = false;
        }

        echo json_encode($array);
    }

    function isPhone($var)
    {
        return preg_match('/^[0-9 ]*$/', $var);
    }

    function isMobile($var)
    {
        return preg_match('/^[0-9 ]*$/', $var);
    }

    function verifyInfo($var)
    {
        $var = trim($var);
        $var = stripslashes($var);
        $var = htmlspecialchars($var);

        return $var;
    }
